package com.example.madina_pc.nozimamarket.activities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

/**
 * Created by Sherzodbek on 06.06.2017.
 */

public class DBHelper extends com.example.madina_pc.nozimamarket.databases.DBHelper {
    private static DBHelper helper;

    public static DBHelper getInstance(Context context) {
        if (helper == null) {
            helper = new DBHelper(context);
        }
        return helper;
    }

    private DBHelper(Context context) {
        super(context, "market.sqlite");
    }


    public Cursor getMainListDatas() {
        return mDatabase.rawQuery("SELECT * FROM lessons", null);
    }

    public void setMainListDatas(int id, String title, String translate) {
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("translate", translate);
        Log.v("TAG", mDatabase.update("lessons", values, "id=" + id, null) + "");
    }

    public void setDatas(int id, String eng, String uzb, byte[] audios) {
        ContentValues values = new ContentValues();
        values.put("eng", eng);
        values.put("uzb", uzb);
        values.put("audio", audios);
        mDatabase.update("datas", values, "id=" + id, null);
    }

    public Cursor getDatas() {
        return mDatabase.rawQuery("SELECT * FROM datas", null);
    }

    public Cursor getColor(int season) {
        return mDatabase.query("season", new String[]{"id", "name", "colour"}, "id=" + season, null, null, null, null);
    }

    public Cursor getType(int season) {
        return mDatabase.query("type", new String[]{"id", "name", "season_id"}, "season_id=" + season, null, null, null, null);
    }
}