package com.example.madina_pc.nozimamarket.activities;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.madina_pc.nozimamarket.R;
import com.example.madina_pc.nozimamarket.adapters.MyRecyclerViewAdapter;
import com.example.madina_pc.nozimamarket.models.Model;
import com.example.madina_pc.nozimamarket.models.TypeModel;

import java.util.ArrayList;

public class TypeActivity extends AppCompatActivity {
    private Intent intent;
    private int season;
    private TextView seasonText;
    private DBHelper dbHelper;
    private RecyclerView recyclerView;
    private ArrayList<Model> types;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        intent = getIntent();
        dbHelper = DBHelper.getInstance(this);
        types = new ArrayList<>();

        seasonText = findViewById(R.id.season);
        recyclerView = findViewById(R.id.type_list);

        season = intent.getIntExtra("season", 0);

        Cursor cursor = dbHelper.getColor(season);
        cursor.moveToFirst();
        String seasonName = cursor.getString(1);
        String color = cursor.getString(2);
        cursor.close();

        Cursor cursor1 = dbHelper.getType(season);
        cursor1.moveToFirst();
        if (!cursor1.isAfterLast()) {
            do {
                types.add(new TypeModel(1, cursor1.getString(1)));
//                Log.i("madina", cursor1.getString(1));

            } while (cursor1.moveToNext());
        }
        cursor1.close();

        seasonText.setTextColor(Color.parseColor(color));

        seasonText.setText(seasonName + " mavsumiga xush kelibsiz!");
        getSupportActionBar().setTitle(seasonName);

        MyRecyclerViewAdapter adapter = new MyRecyclerViewAdapter(types);
        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager manager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(manager);
        Log.i("madina", "setadapter");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
