package com.example.madina_pc.nozimamarket.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.madina_pc.nozimamarket.R;
import com.example.madina_pc.nozimamarket.models.Model;
import com.example.madina_pc.nozimamarket.models.TypeModel;

import java.util.ArrayList;

/**
 * Created by Madina-PC on 13.12.2017.
 */

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Model> data;

    public MyRecyclerViewAdapter(ArrayList<Model> data) {
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_typelist, parent, false);
            Log.i("madina", "layoutni oldi");
            return new ViewHolder1(view);

        } else if (viewType == 2) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_imagelist, parent, false);
            return new ViewHolder2(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Model model = data.get(position);
        int type = model.getType();
        if (type == 1) {
            ViewHolder1 viewHolder1 = (ViewHolder1) holder;
            viewHolder1.type.setText(((TypeModel) model).getTypeCloth());
            Log.i("madina", ((TypeModel) model).getTypeCloth());
        }

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder1 extends RecyclerView.ViewHolder {

        TextView type;

        public ViewHolder1(View itemView) {
            super(itemView);
            type = itemView.findViewById(R.id.type);
        }
    }

    public class ViewHolder2 extends RecyclerView.ViewHolder {
        ImageView image;
        Button productionName;
        Button productionAbout;
        Button productionprice;
        Button productionBuy;

        public ViewHolder2(View itemView) {
            super(itemView);
            productionAbout = itemView.findViewById(R.id.production_about);
            image = itemView.findViewById(R.id.image);
            productionName = itemView.findViewById(R.id.production_name);
            productionprice = itemView.findViewById(R.id.production_price);
            productionBuy = itemView.findViewById(R.id.production_buy);
        }
    }
}
