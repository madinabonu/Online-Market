package com.example.madina_pc.nozimamarket.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.madina_pc.nozimamarket.R;

public class SeasonActivity extends AppCompatActivity {
    private TextView bahor;
    private TextView yoz;
    private TextView kuz;
    private TextView qish;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_season);

        intent = new Intent(this, TypeActivity.class);
        bahor = (TextView) findViewById(R.id.bahor);
        yoz = (TextView) findViewById(R.id.yoz);
        kuz = (TextView) findViewById(R.id.kuz);
        qish = (TextView) findViewById(R.id.qish);

        bahor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("season", 1);
                startActivity(intent);
            }
        });
        yoz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("season", 2);
                startActivity(intent);
            }
        });
        kuz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("season", 3);
                startActivity(intent);
            }
        });
        qish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("season", 4);
                startActivity(intent);
            }
        });
    }

}
