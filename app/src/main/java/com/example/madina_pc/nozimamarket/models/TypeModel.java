package com.example.madina_pc.nozimamarket.models;

/**
 * Created by Madina-PC on 13.12.2017.
 */

public class TypeModel extends Model {

    private String typeCloth;

    public TypeModel(int type, String typeCloth) {
        super(type);
        this.typeCloth = typeCloth;
    }

    public String getTypeCloth() {
        return typeCloth;
    }

    public void setTypeCloth(String typeCloth) {
        this.typeCloth = typeCloth;
    }
}

