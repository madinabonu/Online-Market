package com.example.madina_pc.nozimamarket.models;


/**
 * Created by Madina-PC on 13.12.2017.
 */

public class ImageModel extends Model {

    private int image;
    private String productionName;
    private String productionAbout;
    private String productionPrice;

    public ImageModel(int type, int image, String productionName, String productionAbout, String productionPrice) {
        super(type);
        this.image = image;
        this.productionName = productionName;
        this.productionAbout = productionAbout;
        this.productionPrice = productionPrice;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getProductionName() {
        return productionName;
    }

    public void setProductionName(String productionName) {
        this.productionName = productionName;
    }

    public String getProductionAbout() {
        return productionAbout;
    }

    public void setProductionAbout(String productionAbout) {
        this.productionAbout = productionAbout;
    }

    public String getProductionPrice() {
        return productionPrice;
    }

    public void setProductionPrice(String productionPrice) {
        this.productionPrice = productionPrice;
    }
}
